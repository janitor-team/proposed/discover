/* $Progeny$ */

/**
 * @file stack.h
 * @brief Stack routines for Discover
 */

/*
 * AUTHOR: Josh Bressers <bressers@progeny.com>
 *
 * Copyright 2002 Progeny Linux Systems, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#ifndef STACK_H
#define STACK_H

#include <stdbool.h>

#ifdef __cplusplus
    extern "C" {
#endif

/* Yes, it's not a stack, go away. */

enum action {PUSH, POP, CREATE};
enum status {NOTHING, NEW, CHILD, CLOSE};

typedef struct xml_stack discover_xml_stack;
struct xml_stack
{
    discover_xml_stack *prev;
    void *data;
    int depth;
} ;

discover_xml_stack * discover_xml_stack_new();

void discover_xml_stack_destroy(discover_xml_stack *stack);

void
discover_xml_stack_push(discover_xml_stack **stack, void *data);

void * discover_xml_stack_pop(discover_xml_stack **stack);

void * discover_xml_stack_get(discover_xml_stack *stack);

void * discover_xml_stack_getbynum(discover_xml_stack *stack, int i);

#ifdef __cplusplus
    }
#endif

#endif

/*
 * Local variables:
 * c-file-style: "progeny"
 * indent-tabs-mode: nil
 * End:
 */
/* vim: set cin fo=tcroq sw=4 et sts=4 tw=75: */
