/**
 * @file device.h
 * @brief Private interface for device.c
 */

/* $Progeny$
 *
 * Copyright 2002 Hewlett-Packard Company
 * Copyright 2001, 2002 Progeny Linux Systems, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#ifndef DEVICE_H
#define DEVICE_H


#include <expat.h>

#include <discover/discover.h>

#ifdef __cplusplus
    extern "C" {
#endif

struct discover_data
{
    char *discover_class;
    char *version;
    char *text;

    discover_data_t *parent;
    discover_data_t *child;

    discover_data_t *next;
    discover_data_t *prev;
};

/* If you add or delete members from this structure, be sure to update
 * discover_device_copy in device.c and the detect functions in
 * $BUS.c.
 */
struct discover_device {
    char *busclass;
    char *model_id;
    char *model_name;
    char *vendor_id;
    char *vendor_name;

    discover_xml_busclass_t *busclasses;
    discover_xml_vendor_t *vendors;

    discover_data_t *data;

    discover_device_t *next;  /* Next device that is different */
    discover_device_t *extra; /* Next device that has more info on the same
                               * piece of hardware. */
};

#ifdef __cplusplus
    }
#endif

#endif

/*
 * Local variables:
 * c-file-style: "progeny"
 * indent-tabs-mode: nil
 * End:
 */
/* vim: set cin fo=tcroq sw=4 et sts=4 tw=75: */
