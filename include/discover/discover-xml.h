/**
 * @file discover-xml.h
 * @brief Public interface to Discover's XML handling
 *
 * Public interface that exposes Discover's XML routines.
 * These functions are used to read the XML data 
 * loaded from the specified configuration file(s).
 */

/* $Progeny$
 *
 * AUTHOR: John R. Daily <jdaily@progeny.com>
 *
 * Copyright 2002 Hewlett-Packard Company
 * Copyright 2002 Progeny Linux Systems, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#ifndef DISCOVER_XML_H
#define DISCOVER_XML_H

#include <discover/discover.h>

#ifdef __cplusplus
    extern "C" {
#endif

discover_device_t *
discover_xml_find_device(discover_device_t *xml_devices,
                         char *target_vendor, char *target_model,
                         discover_error_t *status);
discover_device_t *
discover_xml_find_next_device(discover_device_t *xml_devices,
                              char *target_vendor, char *target_model,
                              discover_error_t *status);

discover_device_t *
discover_xml_get_matching_devices(discover_device_t *xml_devices,
                                  char *target_vendor, char *target_model,
                                  discover_error_t *status);

int discover_xml_version_cmp(char *range, char *version,
                             discover_error_t *status);

/******************************************************************************
 * url
 */

discover_xml_url_t *discover_xml_url_new(void);
void discover_xml_url_copy(discover_xml_url_t *src, discover_xml_url_t *dst);
void discover_xml_url_free(discover_xml_url_t *urls);

char *
discover_xml_url_get_url(discover_xml_url_t *url);

char *
discover_xml_url_get_bus(discover_xml_url_t *url);

char *
discover_xml_url_get_type(discover_xml_url_t *url);

discover_xml_url_t *
discover_xml_url_get_next(discover_xml_url_t *url);

discover_xml_url_t *
discover_xml_url_get_last(discover_xml_url_t *url);

char *
discover_xml_url_get_label(discover_xml_url_t *url);

discover_xml_url_t *discover_xml_get_urls(discover_error_t *status);
void discover_xml_free_urls(void);

discover_xml_url_t *discover_xml_get_data_urls(discover_bus_t bus,
                                               discover_filetype_t filetype,
                                               discover_error_t *status);
void discover_xml_free_data_urls(void);

/******************************************************************************
 * busclass
 */

discover_xml_busclass_t *discover_xml_busclass_new(void);

discover_xml_busclass_t *discover_xml_get_busclasses(discover_bus_t bus,
                                                     discover_error_t *status);

void discover_xml_free_busclasses(void);

void
discover_xml_merge_busclass_url(discover_xml_busclass_t **blist,
                                char *filename, discover_error_t *status);

char *discover_xml_busclass_to_class(char *busclass,
                                     discover_xml_busclass_t *busclasses);
int discover_xml_busclass_cmp(char *busclass, char *discover_class,
                              discover_xml_busclass_t *busclasses);
char *discover_xml_busclass_get_id(discover_xml_busclass_t *busclass);

char *discover_xml_busclass_get_name(discover_xml_busclass_t *busclass);
discover_xml_busclass_t *
discover_xml_busclass_get_next(discover_xml_busclass_t *busclass);

void discover_xml_busclass_free(discover_xml_busclass_t *busclasses);

/******************************************************************************
 * device
 */

discover_device_t *
discover_xml_get_devices(discover_bus_t bus, discover_error_t *status);

void discover_xml_free_devices(void);

void
discover_xml_merge_device_url(discover_device_t **dlist, char *url,
                              discover_xml_busclass_t *busclasses,
                              discover_xml_vendor_t *vendors,
                              discover_error_t *status);

/******************************************************************************
 * vendor
 */

discover_xml_vendor_t *discover_xml_vendor_new(void);

void
discover_xml_merge_vendor_url(discover_xml_vendor_t **vlist, char *filename,
                              discover_error_t *status);
discover_xml_vendor_t *discover_xml_get_vendors(discover_bus_t bus,
                                                discover_error_t *status);
void discover_xml_free_vendors(void);

char *discover_xml_vendor_id2name(discover_xml_vendor_t *vendors, char *id);
char *discover_xml_vendor_get_id(discover_xml_vendor_t *vendor);
char *discover_xml_vendor_get_name(discover_xml_vendor_t *vendor);
discover_xml_vendor_t *
discover_xml_vendor_get_next(discover_xml_vendor_t *vendor);

void discover_xml_vendor_free(discover_xml_vendor_t *vendors);

#ifdef __cplusplus
    }
#endif

#endif
