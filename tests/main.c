/* $Progeny$ */

/*
 * Copyright 2002 Hewlett-Packard Company
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include <assert.h>
#include <stdlib.h>
#include <sysexits.h>
#include <stdio.h>
#include <check.h>

#include <discover/discover.h>
#include <discover/sysdep.h>
#include <discover/discover-conf.h>

#include "discover/device.h"

#include "test.h"

static discover_device_t *pci_devices;

unsigned long int ids[] = { 0x102b0010, 0x102b1010, 0x102b2020, 0x102b3030, 0 };

static void
_sysdep_data_init(discover_sysdep_data_t *node)
{
    node->vendor = malloc(5);
    node->model = malloc(5);
}

static discover_sysdep_data_t *
get_pci_devices(void)
{
    discover_sysdep_data_t *head = NULL, *node, *last = NULL;
    int i;

    for (i = 0; ids[i]; i++) {
        node = _discover_sysdep_data_new();
        _sysdep_data_init(node);

        sprintf(node->vendor, "%04x", ids[i] >> 16);
        sprintf(node->model, "%04x", ids[i] & 0xffff);

        if (head == NULL) {
            head = node;
            last = head;
        } else {
            last->next = node;
            last = node;
        }
    }
    return head;
}

int
main(void)
{
    discover_error_t *status;
    discover_bus_map_t *busmap;
    SRunner *sr;
    int i;

    pci_devices = NULL;

    status = discover_error_new();

    busmap = discover_conf_get_full_bus_map(status);
    assert(status->code == 0);

    for (i = 0; busmap[i].name; i++) {
        if (strcmp(busmap[i].name, "pci") == 0) {
            busmap[i].scan_default = 1;
            busmap[i].get_raw = get_pci_devices;
        } else {
            busmap[i].scan_default = 0;
        }
    }
    discover_conf_insert_url("file://list.xml",
                             status);
    assert(status->code == 0);

    sr = srunner_create(make_device_suite());
    srunner_add_suite(sr, make_xml_suite());

    srunner_run_all(sr, CK_NORMAL);

    if (srunner_ntests_failed(sr) != 0) {
        exit(EX_SOFTWARE);
    }

    return 0;
}
