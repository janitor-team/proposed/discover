<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version='1.0'>

<!-- $Progeny$ -->

<!-- Import the LDP customizations of Norm's HTML stylesheet -->
<xsl:import href="/usr/share/sgml/docbook/stylesheet/xsl/ldp/html/tldp-one-page.xsl"/>

<!-- Import our common overrides -->
<xsl:import href="html-common.xsl"/>

</xsl:stylesheet>
