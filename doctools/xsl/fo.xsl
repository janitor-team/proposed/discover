<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                xmlns:doc="http://nwalsh.com/xsl/documentation/1.0"
                exclude-result-prefixes="doc"
                version='1.0'>

<!-- $Progeny$ -->

<!-- We want to rewrite Norm's FO stylesheet, so import it. -->
<xsl:import href="/usr/share/sgml/docbook/stylesheet/xsl/nwalsh/fo/docbook.xsl"/>

<!-- Global XSL customizations -->
<xsl:import href="common.xsl"/>

<!-- K&R as the default doesn't work well with Python.
     ANSI has its own Python issues (void), but overall it seems
     preferable.
-->
<xsl:variable name="funcsynopsis.style">ansi</xsl:variable>

<!-- Without this, the <term> component of a varlistentry is placed on the
     same line as the description, which often leads to overlapping text.
-->
<xsl:variable name="variablelist.as.blocks">1</xsl:variable>

<!-- Enable FOP-specific styling, including bookmarks -->
<xsl:variable name="fop.extensions">1</xsl:variable>

<!-- We have problems with FOP and the DocBook stylesheets; long titles are
     fully justified, which looks pretty ugly.  Here we resolve it in a
     clumsy but effective way.
-->
<xsl:template name="component.title">
  <xsl:param name="node" select="."/>
  <xsl:param name="pagewide" select="0"/>
  <xsl:variable name="id">
    <xsl:call-template name="object.id">
      <xsl:with-param name="object" select="$node"/>
    </xsl:call-template>
  </xsl:variable>
  <xsl:variable name="title">
    <xsl:apply-templates select="$node" mode="object.title.markup">
      <xsl:with-param name="allow-anchors" select="1"/>
    </xsl:apply-templates>
  </xsl:variable>
  <xsl:variable name="titleabbrev">
    <xsl:apply-templates select="$node" mode="titleabbrev.markup"/>
  </xsl:variable>

  <xsl:if test="$passivetex.extensions != 0">
    <fotex:bookmark xmlns:fotex="http://www.tug.org/fotex"
                    fotex-bookmark-level="2"
                    fotex-bookmark-label="{$id}">
      <xsl:value-of select="$titleabbrev"/>
    </fotex:bookmark>
  </xsl:if>

  <fo:block keep-with-next.within-column="always"
            space-before.optimum="{$body.font.master}pt"
            space-before.minimum="{$body.font.master * 0.8}pt"
            space-before.maximum="{$body.font.master * 1.2}pt"
            text-align="start"
            hyphenate="false">
    <!-- Progeny: We added text-align above -->
    <xsl:if test="$pagewide != 0">
      <xsl:attribute name="span">all</xsl:attribute>
    </xsl:if>
    <xsl:copy-of select="$title"/>
  </fo:block>
</xsl:template>

</xsl:stylesheet>
