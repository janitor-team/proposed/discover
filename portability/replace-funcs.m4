# $Progeny$

AC_DEFUN(PROGENY_REPLACE_FUNCS, [
    AC_REPLACE_FUNCS(alloca mkdtemp mkstemp strsep)

    CPPFLAGS="${CPPFLAGS} -I\${top_srcdir}/portability"
    libportability="\${top_builddir}/portability/libportability.la"
    AC_SUBST(libportability)
])
