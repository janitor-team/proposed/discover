/* $Progeny$ */

/* pcmcia.c - Scan the PCMCIA bus
 *
 * Copyright 2002 Hewlett-Packard Company
 * Copyright 2001, 2002 Progeny Linux Systems, Inc.
 * Copyright (C) 1998-2000 MandrakeSoft
 *
 * This file is derived from "cardctl.c" from the pcmcia-cs distribution.
 *
 * Modifications by MandrakeSoft and Progeny Linux Systems, Inc. are
 * licensed as follows:
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*======================================================================

    PCMCIA device control program

    cardctl.c 1.43 1998/12/07 06:04:32

    The contents of this file are subject to the Mozilla Public
    License Version 1.1 (the "License"); you may not use this file
    except in compliance with the License. You may obtain a copy of
    the License at http://www.mozilla.org/MPL/

    Software distributed under the License is distributed on an "AS
    IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
    implied. See the License for the specific language governing
    rights and limitations under the License.

    The initial developer of the original code is David A. Hinds
    <dahinds@users.sourceforge.net>.  Portions created by David A. Hinds
    are Copyright (C) 1999 David A. Hinds.  All Rights Reserved.

    Alternatively, the contents of this file may be used under the
    terms of the GNU General Public License version 2 (the "GPL"), in
    which case the provisions of the GPL are applicable instead of the
    above.  If you wish to allow the use of your version of this file
    only under the terms of the GPL and not to allow others to use
    your version of this file under the MPL, indicate your decision
    by deleting the provisions above and replace them with the notice
    and other provisions required by the GPL.  If you do not delete
    the provisions above, a recipient may use your version of this
    file under either the MPL or the GPL.

======================================================================*/

#include "config.h"

#include <discover/sysdep.h>

#include <stdlib.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/stat.h>

#include <errno.h>
#include <fcntl.h>
#include <ctype.h>

#include <pcmcia/version.h>
#include <pcmcia/cs_types.h>
#include <pcmcia/cs.h>
#include <pcmcia/cistpl.h>
#include <pcmcia/ds.h>


#include <discover/device.h>
#include <discover/utils.h>

/* XXX: There's no reason for this to be global. */
static int major = 0;

static int lookup_dev(char *name){
    FILE *f;
    int n;
    char s[32], t[32];

    f = fopen("/proc/devices", "r");
    if(f == NULL){
        return -errno;
    }   /*endif*/
    while(fgets(s, 32, f) != NULL){
        if(sscanf(s, "%d %s", &n, t) == 2){
            if(strcmp(name, t) == 0){
                break;
            }   /*endif*/
        }   /*endif*/
    }   /*endwhile*/
    fclose(f);
    if (strcmp(name, t) == 0){
        return n;
    }else{
        return -ENODEV;
    }   /*endif*/
}   /*endfunc lookup_dev*/

static int
open_sock(int sock)
{
    int fd;
    char *fn;
    dev_t dev = (major<<8) + sock;
    if((fn = tmpnam(NULL)) == NULL){
        return -1;
    }   /*endif*/
    if(mknod(fn, (S_IFCHR|S_IREAD|S_IWRITE), dev) != 0){
        return -1;
    }   /*endif*/
    fd = open(fn, O_RDONLY);
    unlink(fn);
    return fd;
}

static int get_tuple(int fd, cisdata_t code, ds_ioctl_arg_t *arg){
    arg->tuple.DesiredTuple = code;
    arg->tuple.Attributes = TUPLE_RETURN_COMMON;
    arg->tuple.TupleOffset = 0;
    if((ioctl(fd, DS_GET_FIRST_TUPLE, arg) == 0) &&
        (ioctl(fd, DS_GET_TUPLE_DATA, arg) == 0) &&
        (ioctl(fd, DS_PARSE_TUPLE, arg) == 0)){
        return 0;
    }else{
        return -1;
    }   /*endif*/
}   /*endfunc get_tuple*/

#define MAX_SOCKS 8

/*
 * This function is specific to each sysdep.
 */
static void
_discover_sysdep_init(discover_sysdep_data_t *node)
{
    node->vendor = _discover_xmalloc(5);
    node->model = _discover_xmalloc(5);
}

discover_sysdep_data_t *
_discover_get_pcmcia_raw(void)
{
    int fd, ns;
    ds_ioctl_arg_t arg;
    cistpl_manfid_t *manfid = &arg.tuple_parse.parse.manfid;
    discover_sysdep_data_t *head = NULL, *node, *last = NULL;
    unsigned int id;

    major = lookup_dev("pcmcia");
    if (major < 0) {            /* We don't have a PCMCIA driver. */
        return NULL;
    }

    for (ns = 0; ns < MAX_SOCKS; ns++) {
        fd = open_sock(ns);
        if (fd < 0) {
            break;
        }

        if (get_tuple(fd, CISTPL_MANFID, &arg) == 0) {
            node = _discover_sysdep_data_new();
            _discover_sysdep_init(node);

            id = (manfid->manf * 0x10000) + manfid->card;

            sprintf(node->vendor, "%04x", id >> 16);
            sprintf(node->model, "%04x", id & 0xffff);
#if 0  /* XXX: Do we have a way to extract this? */
            node->busclass = ???;
#endif

            if (head == NULL) {
                head = node;
                last = head;
            } else {
                last->next = node;
                last = node;
            }
        }
        close(fd);
    }

    return head;
}

/*
 * Local variables:
 * c-file-style: "progeny"
 * indent-tabs-mode: nil
 * End:
 */
/* vim: set cin fo=tcroq sw=4 et sts=4 tw=75: */
