/* $Progeny$ */

/* ata.c -- Scan the ATA bus
 *
 * Copyright 2001, 2002 Progeny Linux Systems, Inc.
 * Copyright (C) 1998-2000 MandrakeSoft
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "config.h"

#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <dirent.h>

#include <discover/sysdep.h>

/*
 * This function is specific to each sysdep.
 */
static void
_discover_sysdep_init(discover_sysdep_data_t *node)
{
}

discover_sysdep_data_t *
_discover_get_ata_raw(void)
{
    char path[256];
    char *line;
    char *ide_link;
    FILE *f;
    struct stat state;
    size_t len = 0;
    DIR *ideDir;                   /* Pointer to a directory            */
    struct dirent *ide_link_entry; /* ide_link is a possible link to    */
                                   /* the hdx disk.                     */

    char *model_name, *model_id, *vendor_name;

    discover_sysdep_data_t *first, *last, *node;

    first = last = node = NULL;

    ideDir = opendir(PATH_PROC_IDE);
    if(ideDir == NULL){
        return NULL;
    }/*endif*/

    /* For each link in the PATH_PROC_IDE */
    for (ide_link_entry = readdir(ideDir); ide_link_entry;
         ide_link_entry = readdir(ideDir)) {
        ide_link = ide_link_entry->d_name;
        if (strncmp(ide_link, "hd", 2) == 0) {
            /* Make test for this hdx possible disk. */
            sprintf(path,"%s/%s", PATH_PROC_IDE, ide_link);
            if (!lstat(path, &state)) {
                /* Not so fast!  Make sure that the device isn't being
                 * handled by the IDE-SCSI driver; otherwise, the
                 * device might show up twice (for example, CD burners with
                 * CD-ROM detection).
                 */
                sprintf(path, "%s/%s/driver", PATH_PROC_IDE, ide_link);
                if (!(f = fopen(path, "r"))) {
                    return NULL;
                }
                line = NULL;
                getline(&line, &len, f);
                fclose(f);
                line[8] = 0x00;
                if (!strcmp(line, "ide-scsi")) {
                    continue;
                }

                /* Get vendor and model. */
                sprintf(path, "%s/%s/model", PATH_PROC_IDE, ide_link);
                if (!(f = fopen(path, "r"))) {
                    return NULL;
                }
                getline(&line, &len, f);
                fclose(f);

                /*
                 * Since vendors and models are given as one string,
                 * split them here.  We assume that the first word is
                 * the vendor, which might not necessarily be true, but
                 * is our best guess. 
                 */

                vendor_name = line;
                model_name = line;
                while ((*model_name != ' ') && (*model_name != '\0'))
                    model_name++;
                if (*model_name == ' ')
                    *model_name++ = '\0';

                vendor_name = strdup(vendor_name);

                if (strlen(model_name) > 0) {
                    if (model_name[strlen(model_name) - 1] == '\n')
                        model_name[strlen(model_name) - 1] = '\0'; /* Chomp! */
                    model_name = strdup(model_name);
                } else {
                    model_name = strdup(""); //will be freed
                }

                free(line);
                line = NULL;

                /* Determine the drive type (floppy, hd...). */
                sprintf(path, "%s/%s/media", PATH_PROC_IDE, ide_link);
                if (!(f = fopen(path, "r"))){
                    return NULL;
                }
                getline(&line, &len, f);
                fclose(f);

                model_id = line;
                model_id[strlen(model_id) - 1] = '\0'; /* Chomp! */
                line = NULL;

                /* Initialize the node */
                node = _discover_sysdep_data_new();
                _discover_sysdep_init(node);

                /* Save the node information */
                node->busclass = model_id;
                node->model = model_name;
                node->vendor = vendor_name;

                if (last) {
                    last->next = node;
                    last = node;
                } else {
                    first = last = node;
                }
            }
        }
    }

    closedir(ideDir);
    return first;
}

/*
 * Local variables:
 * c-file-style: "progeny"
 * indent-tabs-mode: nil
 * End:
 */
/* vim: set cin fo=tcroq sw=4 et sts=4 tw=75: */
