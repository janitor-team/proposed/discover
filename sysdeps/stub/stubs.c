/* $Progeny$ */

/**
 * @file stubs.c
 * @brief Stub system-dependent code
 *
 * This file represents a dummy OS-dependent hardware interface.  These
 * routines are the skeleton structure that must be followed to 
 * allow Discover to operate on multiple architectures.
 */

/* stubs.c -- Stubs for Discover device scan routines
 *
 * Copyright 2002 Progeny Linux Systems, Inc.
 * Copyright 2002 Hewlett-Packard Company
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include <config.h>

#include <discover/sysdep.h>

#include <stdio.h>  /* For NULL */

/**
 * @defgroup sysdeps System-dependent interfaces
 * @{
 */

/**
 * Get the list of ATA devices, scanning the bus if necessary.
 */
discover_sysdep_data_t *
_discover_get_ata_raw(void)
{
    return NULL;
}

/**
 * Get the list of PCI devices, scanning the bus if necessary.
 */
discover_sysdep_data_t *
_discover_get_pci_raw(void)
{
    return NULL;
}

/**
 * Get the list of PCMCIA devices, scanning the bus if necessary.
 */
discover_sysdep_data_t *
_discover_get_pcmcia_raw(void)
{
    return NULL;
}

/**
 * Get the list of SCSI devices, scanning the bus if necessary.
 */
discover_sysdep_data_t *
_discover_get_scsi_raw(void)
{
    return NULL;
}

/**
 * Get the list of USB devices, scanning the bus if necessary.
 */
discover_sysdep_data_t *
_discover_get_usb_raw(void)
{
    return NULL;
}

/** @} */

/*
 * Local variables:
 * c-file-style: "progeny"
 * indent-tabs-mode: nil
 * End:
 */
/* vim: set cin fo=tcroq sw=4 et sts=4 tw=75: */
