# Copyright 2002 Hewlett-Packard Company
# Copyright 2002 Progeny Linux Systems, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE COPYRIGHT HOLDER(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

AC_INIT(discover, 2.1.2, [debian-boot@lists.debian.org])
AC_REVISION([$Progeny$])

AC_PREREQ(2.53)
# There's a bug in autoconf 2.53 when creating
# abs_top_builddir from top_builddir.  We have autoconf in our CVS
# repository, where this bug has been fixed.  GNU cvs has a similar fix,
# but lots of other changes.

AC_CONFIG_AUX_DIR(buildtools)
AC_CONFIG_HEADERS(config.h)
AC_CONFIG_SRCDIR(config.h.in)

AC_COPYRIGHT([Copyright 2002 Progeny Linux Systems, Inc.])

# Set variables for canonical build, host, and target system type.
AC_CANONICAL_BUILD()
AC_CANONICAL_HOST()
AC_CANONICAL_TARGET()

###############################################################################
# Versioning

# LT_CURRENT is the major version number of the library (SONAME).  It
# must be incremented every time an incompatible change is made.  When
# LT_CURRENT is incremented, PACKAGE_MAJOR must also be incremented
# (and PACKAGE_{MINOR,MICRO} reset to 0).  LT_CURRENT and
# PACKAGE_MAJOR must always be equal.

# LT_{REVISION,AGE} are not related to PACKAGE_{MINOR,MICRO}.  The
# former are managed according to libtool's versioning spec and the
# latter according to Progeny release policy.

# Package version
PACKAGE_MAJOR=2
PACKAGE_MINOR=1
PACKAGE_MICRO=0

# Library version
LT_CURRENT=2
LT_REVISION=1
LT_AGE=0

###############################################################################
# Arguments

AC_ARG_WITH(default-url,
[  --with-default-url=FOO  Use FOO for the default data source URL.],
DISCOVER_DEFAULT_URL=${withval}
)

AC_ARG_WITH(pcmcia-headers,
[  --with-pcmcia-headers=DIR Directory containing PCMCIA headers
                            like <pcmcia/version.h>],
if test x$withval = xyes; then
    AC_MSG_ERROR([--with-pcmcia-headers must be given a pathname.])
else
    pcmcia_headers=${withval}
fi
)

AC_ARG_WITH(sysdeps,
[  --with-sysdeps=FOO      Use the code in the directory FOO for system-dependent
                          interfaces.],
SYSDEPS=sysdeps/${withval}
if test $withval = linux; then
    LINUXSYSDEP=libsysdeps.la
else
    STUBSYSDEP=libsysdeps.la
fi
)

AC_ARG_ENABLE(curl,
[  --enable-curl           Use curl to read configuration and hardware data.],
ENABLE_CURL=$enableval,
ENABLE_CURL=probe
)

###############################################################################
# Checks for programs

AC_PROG_CC_STDC()
AC_PROG_CPP()

AC_PROG_INSTALL()
if test "${INSTALL_DIR}" = ""; then
    INSTALL_DIR="${INSTALL} -m 755 -d"
fi

AC_PROG_LIBTOOL()

if test "${DOXYGEN}" = ''; then
   AC_PATH_PROG(DOXYGEN, doxygen, false)
fi

if test "${CURL_CONFIG}" = ''; then
   AC_PATH_TOOL(CURL_CONFIG, curl-config, false)
fi

###############################################################################
# Checks for libraries and functions

PROGENY_REPLACE_FUNCS()

AC_HEADER_DIRENT()

AC_CHECK_HEADER(expat.h, , AC_MSG_ERROR([Can't find expat.h.]))
AC_CHECK_LIB(expat, XML_ParserCreate, ,
             AC_MSG_ERROR([Can't find expat library.]))

# curl

if test "${ENABLE_CURL}" '!=' "no"; then
    AC_DEFINE(HAVE_LIBCURL, 1,
              [Define to 1 if you have the `curl' library (-lcurl).])

    # Unfortunately, curl-config --libs returns both LDFLAGS and LIBS
    # and it would be difficult to separate them.  Just dump it all into
    # LIBS.
    AC_MSG_CHECKING([for curl libs])
    curl_libs=`${CURL_CONFIG} --libs`
    if test $? -ne 0; then
        AC_MSG_ERROR([curl-config --libs failed.])
    fi

    # More curl workarounds.  Adding the system include path with -I and/or
    # system library path with -L causes problems on some systems.
    curl_libs=`echo ${curl_libs} | sed -e 's|-L/usr/lib||g'`

    AC_MSG_RESULT(${curl_libs})

    # Yet more curl madness.  curl-config --cflags returns CPPFLAGS only, so
    # let's put it into CPPFLAGS where it belongs.
    AC_MSG_CHECKING([for curl cflags])
    curl_cflags=`${CURL_CONFIG} --cflags`
    if test $? -ne 0; then
        AC_MSG_ERROR([curl-config --cflags failed.])
    fi

    curl_cflags=`echo ${curl_cflags} | sed -e 's|-I/usr/include||g'`

    AC_MSG_RESULT(${curl_cflags})

    LIBS="${LIBS} ${curl_libs}"
    CPPFLAGS="${CPPFLAGS} ${curl_cflags}"
fi

# check

AM_PATH_CHECK(0.8.2, , true)
if test "x${CHECK_LIBS}" = "x"; then
    # Try to reassure the user after check's stupid error message.
    echo "You will not be able to run the test suite, no worries."
fi

###############################################################################
# Miscellaneous

AC_C_BIGENDIAN

if test ${datadir} = '${prefix}/share'; then
    if test "${prefix}" = "NONE"; then
        DATADIR="${ac_default_prefix}/share"
    else
        DATADIR="${prefix}/share"
    fi
else
    DATADIR="${datadir}"
fi

if test ${sysconfdir} = '${prefix}/etc'; then
    if test "${prefix}" = "NONE"; then
        SYSCONFDIR="${ac_default_prefix}/etc"
    else
        SYSCONFDIR="${prefix}/etc"
    fi
else
    SYSCONFDIR="${sysconfdir}"
fi

if test "x${DISCOVER_DEFAULT_URL}" = "x"; then
    DISCOVER_DEFAULT_URL=file://${DATADIR}/${PACKAGE_NAME}/list.xml
fi

case ${target_os} in
linux*)
    if test "x${SYSDEPS}" = "x"; then
        SYSDEPS=sysdeps/linux
        LINUXSYSDEP=libsysdeps.la
    fi

    if test "${SYSDEPS}" = "sysdeps/linux"; then
        AC_DEFINE(_GNU_SOURCE, 1,
                  [Enable all GNU extensions.])

        PATH_PROC_IDE="/proc/ide"
        PATH_PROC_PCI="/proc/bus/pci/devices"
        PATH_PROC_PCI_DIR=`dirname $PATH_PROC_PCI`
        PATH_SYS_PCI="/sys/bus/pci/devices"
        PATH_PROC_SCSI="/proc/scsi/scsi"
        PATH_PROC_USB="/proc/bus/usb/devices"
        AC_DEFINE_UNQUOTED(PATH_PROC_IDE, "$PATH_PROC_IDE",
                           [Define path to /proc/ide.])
        AC_DEFINE_UNQUOTED(PATH_PROC_PCI, "$PATH_PROC_PCI",
                           [Define path to /proc/bus/pci/devices.])
        AC_DEFINE_UNQUOTED(PATH_PROC_PCI_DIR, "$PATH_PROC_PCI_DIR",
                           [Define path to /proc/bus/pci.])
        AC_DEFINE_UNQUOTED(PATH_SYS_PCI, "$PATH_SYS_PCI",
                           [Define path to /sys/bus/pci/devices.])
        AC_DEFINE_UNQUOTED(PATH_PROC_SCSI, "$PATH_PROC_SCSI",
                           [Define path to /proc/scsi/scsi.])
        AC_DEFINE_UNQUOTED(PATH_PROC_USB, "$PATH_PROC_USB",
                           [Define path to /proc/bus/usb/devices.])

        if test "x${pcmcia_headers}" = "x"; then
#            found=no
#            for pcmcia_headers in "/lib/modules/`uname -r`/build/include"   \
#                                  "/usr/src/kernel-headers-`uname -r`/include" \
#                              /usr/src/linux*/include                   \
#                              /usr/local/src/linux*/include
#            do
#                AC_MSG_CHECKING([for PCMCIA headers in ${pcmcia_headers}])
#                if test -f "${pcmcia_headers}/pcmcia/version.h"; then
#                    found=yes
#                    AC_MSG_RESULT([yes])
#                    break
#                else
#                    AC_MSG_RESULT([no])
#                fi
#            done
#            if test ${found} = no; then
#                    AC_MSG_ERROR([Could not find PCMCIA headers.])
#            fi
            pcmcia_headers="`cd ${srcdir} && pwd`/${SYSDEPS}"
        else
            AC_MSG_CHECKING([for PCMCIA headers in ${pcmcia_headers}])
            if ! test -f "${pcmcia_headers}/pcmcia/version.h"; then
                AC_MSG_RESULT([no])
                AC_MSG_ERROR([No PCMCIA headers in ${pcmcia_headers} .])
            else
                AC_MSG_RESULT([yes])
            fi
        fi

        CPPFLAGS="${CPPFLAGS} -I${pcmcia_headers}"
    fi
    ;;

*)
    if test "x${SYSDEPS}" = "x"; then
        SYSDEPS=sysdeps/stub
        STUBSYSDEP=libsysdeps.la
    fi
    ;;
esac

AC_MSG_CHECKING([for sysdeps in ${SYSDEPS}])
if test -f ${srcdir}/${SYSDEPS}/Makefile.in; then
    AC_MSG_RESULT([yes])
else
    AC_MSG_ERROR([Could not find ${SYSDEPS}.])
fi

# For discover-config(1).
export_LIBS="-ldiscover ${LIBS}"
case ${prefix} in
NONE)
    export_CPPFLAGS="-I${ac_default_prefix}/include"
    export_LDFLAGS="-L${ac_default_prefix}/lib -Wl,-R${ac_default_prefix}/lib"
    ;;
/usr)
    export_CPPFLAGS=""
    export_LDFLAGS=""
    ;;
*)
    export_CPPFLAGS="-I${prefix}/include"
    export_LDFLAGS="-L${prefix}/lib -Wl,-R${prefix}/lib"
esac

###############################################################################
# Output

CPPFLAGS="${CPPFLAGS} -I\${top_srcdir} -I\${top_builddir}"
CPPFLAGS="${CPPFLAGS} -I\${top_srcdir}/include"

LTLIBOBJS=`echo "$LIB@&t@OBJS" | sed 's,\.[[^.]]* ,.lo ,g;s,\.[[^.]]*$,.lo,'`

AC_DEFINE_UNQUOTED(DISCOVER_DEFAULT_URL, "${DISCOVER_DEFAULT_URL}",
                   [Define URL for default list of data files.])

# 'TARGET' sounds like a potential namespace collision, so prefix it.
AC_DEFINE_UNQUOTED(DISCOVER_TARGET, "${target}",
                   [Target CPU, vendor, and OS])

AC_DEFINE_UNQUOTED(PACKAGE_MAJOR, ${PACKAGE_MAJOR},
                   [Discover major version number])
AC_DEFINE_UNQUOTED(PACKAGE_MINOR, ${PACKAGE_MINOR},
                   [Discover minor version number])
AC_DEFINE_UNQUOTED(PACKAGE_MICRO, ${PACKAGE_MICRO},
                   [Discover micro version number])

AC_DEFINE_UNQUOTED(SYSCONFDIR, "${SYSCONFDIR}",
                   [Path to system configuration directory])

AC_SUBST(PACKAGE_MAJOR)
AC_SUBST(PACKAGE_MINOR)
AC_SUBST(PACKAGE_MICRO)
AC_SUBST(LT_CURRENT)
AC_SUBST(LT_REVISION)
AC_SUBST(LT_AGE)

AC_SUBST(INSTALL_DIR)
AC_SUBST(LIBS)
AC_SUBST(LTLIBOBJS)
AC_SUBST(SYSDEPS)
AC_SUBST(LINUXSYSDEP)
AC_SUBST(STUBSYSDEP)
AC_SUBST(export_CPPFLAGS)
AC_SUBST(export_LDFLAGS)
AC_SUBST(export_LIBS)

AC_SUBST(CURL_CONFIG)

AC_SUBST(DISCOVER_DEFAULT_URL)

PROGENY_CONFIG_SCRIPT(discover-config)
PROGENY_CONFIG_SCRIPT_VAR(lt_current, ${LT_CURRENT})
PROGENY_CONFIG_SCRIPT_VAR(lt_revision, ${LT_REVISION})
PROGENY_CONFIG_SCRIPT_VAR(lt_age, ${LT_AGE})

# standard parameters
PROGENY_SHOW_CONFIG()
PROGENY_SHOW_CONFIG_VAR(CFLAGS, ${CFLAGS})
PROGENY_SHOW_CONFIG_VAR(CPPFLAGS, ${CPPFLAGS})
PROGENY_SHOW_CONFIG_VAR(LDFLAGS, ${LDFLAGS})
PROGENY_SHOW_CONFIG_VAR(LIBS, ${LIBS})
PROGENY_SHOW_CONFIG_VAR(LTLIBOBJS, ${LTLIBOBJS})
PROGENY_SHOW_CONFIG_VAR(DISCOVER_DEFAULT_URL, ${DISCOVER_DEFAULT_URL})
PROGENY_SHOW_CONFIG_VAR(SYSDEPS, ${SYSDEPS})

# OS-specific parameters
case ${target_os} in
linux*)
    if test "${SYSDEPS}" = "sysdeps/linux"; then
        PROGENY_SHOW_CONFIG_VAR(pcmcia_headers, ${pcmcia_headers})
    fi
        ;;
esac

AC_CONFIG_FILES([
Makefile:buildtools/recur.mk:buildtools/build.mk:Makefile.in
buildtools/Makefile
discover/Makefile:buildtools/build.mk:discover/Makefile.in
discover-xml/Makefile:buildtools/build.mk:discover-xml/Makefile.in
doc/Makefile:buildtools/build.mk:doc/Makefile.in
doctools/Makefile
etc/Makefile:buildtools/build.mk:etc/Makefile.in
portability/Makefile
include/Makefile:buildtools/recur.mk:buildtools/build.mk:include/Makefile.in
include/discover/Makefile:buildtools/build.mk:include/discover/Makefile.in
lib/Makefile:buildtools/build.mk:lib/Makefile.in
scripts/Makefile:buildtools/build.mk:scripts/Makefile.in
sysdeps/Makefile:buildtools/build.mk:sysdeps/Makefile.in:buildtools/recur.mk
sysdeps/linux/Makefile:buildtools/recur.mk:buildtools/build.mk:sysdeps/linux/Makefile.in
sysdeps/linux/pcmcia/Makefile:buildtools/build.mk:sysdeps/linux/pcmcia/Makefile.in
sysdeps/stub/Makefile:buildtools/build.mk:sysdeps/stub/Makefile.in
tests/Makefile:buildtools/build.mk:tests/Makefile.in
])

AC_OUTPUT()
