# $Progeny$

__recur:
	@for dir in ${SUBDIR}; do					\
		echo "${TARGET} ===> ${_THISDIR_}$${dir}";		\
		cd "$${dir}" &&						\
		${MAKE} "_THISDIR_=${_THISDIR_}$${dir}/" ${TARGET}	\
			|| exit $$?;					\
		cd ${abs_builddir};					\
	done
