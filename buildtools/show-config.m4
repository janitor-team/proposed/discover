# $Progeny$

AC_DEFUN(PROGENY_SHOW_CONFIG_VAR, [

PROGENY_SHOW_CONFIG_VARS="${PROGENY_SHOW_CONFIG_VARS}; VARS=\"\${VARS} $1\""
PROGENY_SHOW_CONFIG_SET="${PROGENY_SHOW_CONFIG_SET}; $1=\"$2\""
])

AC_DEFUN(PROGENY_SHOW_CONFIG, [

dnl Initialize these so they don't start with a semicolon, since some
dnl shells react badly to a leading semicolon.
PROGENY_SHOW_CONFIG_VARS=":"
PROGENY_SHOW_CONFIG_SET=":"

AC_SUBST(build)
AC_SUBST(host)
AC_SUBST(target)
AC_SUBST(PROGENY_SHOW_CONFIG_SET)
AC_SUBST(PROGENY_SHOW_CONFIG_VARS)

AC_CONFIG_FILES(buildtools/show-config,
                chmod 555 buildtools/show-config)
AC_CONFIG_COMMANDS(show-config,
                   ./buildtools/show-config)
])
