# $Progeny$

AC_DEFUN(PROGENY_CONFIG_SCRIPT_VAR, [

PROGENY_CONFIG_SCRIPT_VARS="${PROGENY_CONFIG_SCRIPT_VARS}; VARS=\"\${VARS} $1\""
PROGENY_CONFIG_SCRIPT_SET="${PROGENY_CONFIG_SCRIPT_SET}; $1=\"$2\""

PROGENY_CONFIG_SCRIPT_DOC="${PROGENY_CONFIG_SCRIPT_DOC}
      <listitem>
        <para><varname>$1</varname></para>
      </listitem>
"
])

AC_DEFUN(PROGENY_CONFIG_SCRIPT, [

ifelse($1, [],
    PROGENY_CONFIG_SCRIPT_NAME=${PACKAGE_TARNAME}-config
,
    PROGENY_CONFIG_SCRIPT_NAME="$1"
)

dnl Initialize these so they don't start with a semicolon, since some
dnl shells react badly to a leading semicolon.
PROGENY_CONFIG_SCRIPT_VARS=":"
PROGENY_CONFIG_SCRIPT_SET=":"

PROGENY_CONFIG_SCRIPT_VAR(major_version, ${PACKAGE_MAJOR})
PROGENY_CONFIG_SCRIPT_VAR(minor_version, ${PACKAGE_MINOR})
PROGENY_CONFIG_SCRIPT_VAR(micro_version, ${PACKAGE_MICRO})
PROGENY_CONFIG_SCRIPT_VAR(version,
                          \${major_version}.\${minor_version}.\${micro_version})
PROGENY_CONFIG_SCRIPT_VAR(cppflags, ${export_CPPFLAGS})
PROGENY_CONFIG_SCRIPT_VAR(ldflags, ${export_LDFLAGS})
PROGENY_CONFIG_SCRIPT_VAR(libs, ${export_LIBS})

for var in prefix \
           exec_prefix; do
    eval val="\$${var}"
    if test "${val}" = "NONE"; then
        val=${ac_default_prefix}
    fi
    PROGENY_CONFIG_SCRIPT_VAR(${var}, ${val})
done

for var in bindir \
         sbindir \
         libexecdir \
         datadir \
         sysconfdir \
         sharedstatedir \
         localstatedir \
         libdir \
         includedir \
         oldincludedir \
         infodir \
         mandir \
         build \
         build_cpu \
         build_vendor \
         build_os \
         host \
         host_cpu \
         host_vendor \
         host_os \
         target \
         target_cpu \
         target_vendor \
         target_os; do
    eval val="\$${var}"
    PROGENY_CONFIG_SCRIPT_VAR(${var}, ${val})
done

AC_SUBST(PROGENY_CONFIG_SCRIPT_NAME)
AC_SUBST(PROGENY_CONFIG_SCRIPT_SET)
AC_SUBST(PROGENY_CONFIG_SCRIPT_VARS)

AC_CONFIG_FILES(buildtools/config-script)

AC_CONFIG_COMMANDS(config-script-doc,
                   echo "${doctext}"                                    \
                       | ${ac_top_srcdir}/${mkdoc}                      \
                           ${refentry}                                  \
                           ${docname}
                   ,
                   doctext="${PROGENY_CONFIG_SCRIPT_DOC}";
                   mkdoc='buildtools/config-script-mkdoc';
                   refentry='buildtools/config-script.refentry';
                   docname="${PROGENY_CONFIG_SCRIPT_NAME}")
])
