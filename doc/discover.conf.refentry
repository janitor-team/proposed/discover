<!--
Copyright 2002 Hewlett-Packard Company

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE COPYRIGHT HOLDER(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
-->

  <refentryinfo>
    <releaseinfo>$Progeny$</releaseinfo>

    <authorgroup>
      &author.jdaily;
    </authorgroup>
    <copyright>
      <year>2002</year>
      <holder>&progeny.inc;</holder>
    </copyright>
  </refentryinfo>

  <refmeta>
    <refentrytitle>discover.conf</refentrytitle>
    <manvolnum>5</manvolnum>
  </refmeta>

  <refnamediv>
    <refname>discover.conf</refname>
    <refpurpose>configuration file format for discover(1)</refpurpose>
  </refnamediv>

  <refsect1 id="dc5-rs-description">
    <title id="dc5-rs-title-description">Description</title>

    <para>&discover; looks for configuration files in a configuration
    directory, containing a number of files.  These define the 
    system buses that should be scanned by default, those that should 
    never be scanned, and the <acronym>URL</acronym>s for hardware data 
    files beyond the local copy provided with the software.</para>

    <para>The file format is &xml;; the <link
    linkend="ap-discover_conf_dtd"><acronym>DTD</acronym></link>
    is provided with the &discover; software, and can be used for
    informational or validation purposes.</para>

  </refsect1>

  <refsect1 id="dc5-rs-examples">
    <title id="dc5-title-rs-examples">Examples</title>

    <example>
      <title>Establishing default buses to scan</title>
      <programlisting><![CDATA[
<?xml version="1.0"?>

<!DOCTYPE conffile SYSTEM "conffile.dtd">

<conffile>
  <busscan scan="default">
    <bus name="ata"/>
    <bus name="pci"/>
    <bus name="pcmcia"/>
    <bus name="scsi"/>
    <bus name="usb"/>
  </busscan>
</conffile>
]]>
</programlisting>
    </example>

    <example>
      <title>A more complex example</title>
      <programlisting><![CDATA[
<?xml version="1.0"?>

<!DOCTYPE conffile SYSTEM "conffile.dtd">

<conffile>
  <busscan scan="default">
    <bus name="ata"/>
    <bus name="pci"/>
    <bus name="pcmcia"/>
    <bus name="usb"/>
  </busscan>
  <!-- My ancient SCSI card locks up when probed -->
  <busscan scan="never">
    <bus name="scsi"/>
  </busscan>
  <data-sources>
    <data-source url="http://www.example.com/discover/xfree86.xml"
                 label="Updated XFree86 hardware information">
  </data-sources>
</conffile>
]]>
</programlisting>

    </example>
  </refsect1>

  <refsect1>
    <title>Authors</title>

    <para>&name.bress;, &name.jdaily;, and
    &name.branden; developed the current implementation of
    &discover; for Progeny Linux Systems.</para>

    <para>The Linux implementation of the system-dependent interfaces is
    derived from <command>detect</command>, by MandrakeSoft SA.</para>
  </refsect1>

  <refsect1 id="dc5-rs-see-also">
    <title id="dc5-rs-title-see-also">See Also</title>

    <para>discover(1)</para>

  </refsect1>

<!-- Local variables: -->
<!-- mode: xml -->
<!-- sgml-parent-document: ("guide.xml" "refentry" "refentry") -->
<!-- End: -->
<!-- vim: set ai et sts=2 sw=2 tw=65: -->
